import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Network } from 'ionic-native';
import { Platform } from 'ionic-angular';
import 'rxjs/add/operator/map';
import firebase from 'firebase';

@Injectable()
export class ConnectivityProvider {

	public fireAuth: any;
	public userProfile: any;
	onDevice: boolean;

	constructor(public http: HttpClient, public platform: Platform) {
		this.fireAuth = firebase.auth();
		this.userProfile = firebase.database().ref('users');
		this.onDevice = this.platform.is('cordova');
	}

	isOnline(): boolean {
		if(this.onDevice && Network.type){
			return Network.type != 'none';
		} else {
			return navigator.onLine; 
		}
	}

	isOffline(): boolean {
		if(this.onDevice && Network.type){
			return Network.type == 'none';
		} else {
			return !navigator.onLine;   
		}
	}

	signUpUser(pseudo: string, email: string, phone: string, password: string) {
		// Création de l'utilisateur
		return this.fireAuth.createUserWithEmailAndPassword(email, password)
		.then((newUser) => {
			// Authentification
			this.fireAuth.signInWithEmailAndPassword(email, password)
			.then((authenticatedUser) => {
				// Création du profil utilisateur dans la BDD 'users'
				this.userProfile.child(authenticatedUser.uid).set({
					pseudo: pseudo,
					email: email,
					phone: phone
				})
			})
		})
	}

	loginUser(email: string, password: string) {
		return this.fireAuth.signInWithEmailAndPassword(email, password);
	}

	logoutUser() {
		return this.fireAuth.signOut();
	}

	forgotPasswordUser(email: any) {
		return this.fireAuth.sendPasswordResetEmail(email);
	}

	viewUser(userId: any) {
		var userRef = this.userProfile.child(userId);
		return userRef.once('value');
	}
}
