import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import * as firebase from 'firebase';

@Injectable()
export class ActivityServiceProvider {
  private activities: any;

  constructor(public http: Http) {
    this.activities = firebase.database().ref('sorties');
  }

  viewActivity(activityId: any) {
    var activityRef = this.activities.child(activityId);
    return activityRef.once('value');
  }

  addMemberInActivity(activityId: any, userId: any, userProfile: any) {
	this.activities.child(activityId).child('participants').child(userId).set({
		name: 'test'
	});
  }
}

