import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { HttpClientModule } from '@angular/common/http'; 
import { HttpModule } from '@angular/http';
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { UserDetailsPage } from '../pages/user-details/user-details';
import { AddPage } from '../pages/add/add';
import { Geolocation } from '@ionic-native/geolocation';
import { TemperaturePipe } from '../pipes/temperature/temperature';
import { ConnectivityProvider } from '../providers/connectivity/connectivity';
import { LoginPageModule } from '../pages/login/login.module';
import { TabsWeatherPage } from '../pages/tabs-weather/tabs-weather';
import { LocationsProvider } from '../providers/locations/locations';
import { GoogleMapsProvider } from '../providers/google-maps/google-maps';
import { ActivityServiceProvider } from '../providers/activity-service/activity-service';


@NgModule({
  declarations: [
    MyApp,
    HomePage,
    AddPage,
    TemperaturePipe,
    TabsWeatherPage,
    UserDetailsPage,
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpModule,
    HttpClientModule,
    LoginPageModule,
    //ComponentsModule,
  ],
  exports: [
    //ComponentsModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    AddPage,
    TabsWeatherPage,
    UserDetailsPage,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    Geolocation,
    ConnectivityProvider,
    LocationsProvider,
    GoogleMapsProvider,
    ActivityServiceProvider,
  ]
})
export class AppModule {}
