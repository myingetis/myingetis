import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { LoginPage } from '../pages/login/login';
import { TabsWeatherPage } from '../pages/tabs-weather/tabs-weather';
import firebase from 'firebase';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen) {

    var config = {
    apiKey: "AIzaSyC-CYv3H7ttWy6cjns_B8xOSlOXUqvI0gY",
    authDomain: "my-ingetis.firebaseapp.com",
    databaseURL: "https://my-ingetis.firebaseio.com",
    projectId: "my-ingetis",
    storageBucket: "my-ingetis.appspot.com",
    messagingSenderId: "486454871003"
    };
    firebase.initializeApp(config);

    firebase.auth().onAuthStateChanged((user) => {
      if (user && user.uid) {
        this.rootPage = TabsWeatherPage;
      }
      else {
        this.rootPage = LoginPage;
      }
    });

    platform.ready().then(() => {
      statusBar.styleDefault();
      splashScreen.hide();
    });
  }
}

