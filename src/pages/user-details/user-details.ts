import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ConnectivityProvider } from '../../providers/connectivity/connectivity';

@IonicPage()
@Component({
  selector: 'page-user-details',
  templateUrl: 'user-details.html',
  providers: [ConnectivityProvider]
})
export class UserDetailsPage {

  constructor(public navCtrl: NavController, public navParams: NavParams, private connectivityServices: ConnectivityProvider) {
  }

  logUserOut() {
    this.connectivityServices.logoutUser()
    .then(() => {
      this.navCtrl.setRoot('LoginPage');
    });
  }

  ViewAdvancedSettings() {
      this.navCtrl.push('AdvancedSettingsPage');
  }
}
