import { Component } from '@angular/core';
import { IonicPage, ViewController } from 'ionic-angular';
import { ConnectivityProvider } from '../../providers/connectivity/connectivity';

import firebase from 'firebase';

@IonicPage()
@Component({
  selector: 'page-add',
  templateUrl: 'add.html',
  providers: [ConnectivityProvider]
})
export class AddPage {
	activities :any;
	userId: any;
	userName: any;
	conditionsExp: any;
	nameField : any;
	dateField : any;
	locationField : any;
	descriptionField: any;

	constructor(public view: ViewController, private connectivityServices: ConnectivityProvider) {
		this.activities = firebase.database().ref('sorties');
		var myUserId = firebase.auth().currentUser.uid;
		this.displayUser(myUserId);
	}

	displayUser(theUserId) {
		this.connectivityServices.viewUser(theUserId)
		.then(snapshot => {
			this.userName = snapshot.val().pseudo;
		})
	}

	addActivity(nameField, dateField, locationField) {
		this.activities.push({
			name: this.nameField,
			date: this.dateField,
			address: this.locationField,
			description: this.descriptionField,
			organisateur: this.userName,
			participants: ""
		});

		this.view.dismiss();
  	}

	dismiss(formData) {
		this.view.dismiss(formData);
	}
}
