import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TabsWeatherPage } from './tabs-weather';

@NgModule({
  declarations: [
    TabsWeatherPage,
  ],
  imports: [
    IonicPageModule.forChild(TabsWeatherPage),
  ]
})
export class TabsWeatherPageModule {}
