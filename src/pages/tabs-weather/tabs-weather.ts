import { Component } from '@angular/core';
import { HomePage } from '../home/home';
import { UserDetailsPage } from '../user-details/user-details';

@Component({
  templateUrl: 'tabs-weather.html'
})
export class TabsWeatherPage {

  tab1Root = HomePage;
  tab2Root = UserDetailsPage;

  constructor() {

  }
}