import { Component, ElementRef, ViewChild } from '@angular/core';
import { ModalController, NavController, Platform, Events } from 'ionic-angular';
import { LocationsProvider } from '../../providers/locations/locations';
import { GoogleMapsProvider } from '../../providers/google-maps/google-maps';
import { AddPage } from '../add/add';
import { ActivityDetailsPage } from '../activity-details/activity-details';
//import firebase from 'firebase';
import 'rxjs/add/operator/map';

@Component({
	selector: 'page-home',
	templateUrl: 'home.html',
	providers: [],
})
export class HomePage {
	//private userId: any;
	@ViewChild('map') mapElement: ElementRef;
    @ViewChild('pleaseConnect') pleaseConnect: ElementRef;

	constructor(public modalCtrl: ModalController, public navCtrl: NavController, public platform: Platform, public maps: GoogleMapsProvider, public locations: LocationsProvider, public events: Events) {
		//this.userId = firebase.auth().currentUser.uid;
	}

	ionViewDidLoad(){
		this.platform.ready().then(() => {

            let mapLoaded = this.maps.init(this.mapElement.nativeElement, this.pleaseConnect.nativeElement);
            let locationsLoaded = this.locations.load();

            Promise.all([
                mapLoaded,
                locationsLoaded
            ]).then((result) => {

                let locations = result[1];
                var i = 0;
                for(var location in locations){
                    this.maps.addMarker(locations[location].localisation.latitude, locations[location].localisation.longitude, Object.keys(locations)[i]);
                    i++;
                }
            });
        });

        this.events.subscribe('marker:click', (key) => {
            this.navCtrl.push('ActivityDetailsPage', {
                param1: key
            });
        });
  	}

	addActivity() {
		let addActivityModal = this.modalCtrl.create(AddPage);
		addActivityModal.onDidDismiss( (data) => {
			if (data) {
				console.log(data);
				//this.getWeather(data.city, data.country);
			}
		});
		addActivityModal.present();
	}

	/*getWeather(city: any, country: any) {
		// get weather from api
		this.weather.city(city,country)
		//.map(data => data.json())
		.subscribe(data => {
			this.weatherList.push(data);
		},
		err => console.log(err),
		() => console.log('getWeather'))
	}*/
}
