import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ActivityServiceProvider } from '../../providers/activity-service/activity-service';
import { ConnectivityProvider } from '../../providers/connectivity/connectivity';

import firebase from 'firebase';


@IonicPage()
@Component({
  selector: 'page-activity-details',
  templateUrl: 'activity-details.html',
  providers: [ActivityServiceProvider, ConnectivityProvider]
})
export class ActivityDetailsPage {
	private activityId: any;
	private userId: any;

	constructor(public navCtrl: NavController, public navParams: NavParams, private activityService: ActivityServiceProvider, private connectivityServices: ConnectivityProvider) {
		this.userId = firebase.auth().currentUser.uid;
		this.activityId = navParams.get('param1');
		this.displayActivity(this.activityId);
	}

	displayActivity(theActivityId) {
	this.activityService.viewActivity(theActivityId)
	  .then(snapshot => {
	    this.address = snapshot.val().address;
	    this.date = snapshot.val().date;
	    this.name = snapshot.val().name;
	    this.desc = snapshot.val().description;
	  })
	}

	addMember() {
		this.connectivityServices.viewUser(this.userId)
		.then(snapshot => {
			this.userName = snapshot.val().pseudo;
			//this.phoneNumber = snapshot.val().phone;
			//this.emailAddress = snapshot.val().email;
		});

		this.activityService.addMemberInActivity(this.activityId, this.userId);
	}
}
